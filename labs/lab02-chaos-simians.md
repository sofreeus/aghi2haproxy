# Chaotic Simians

c.f. Chaos Monkey, Chaos Gorilla

Turn off a few httpd containers on each side. Note that the 'site' stays up, haproxy logs the failure, and watches for recovery.

```bash
# monkey - "crash" the red green and indigo nodes
docker-compose stop red
docker-compose stop green
docker-compose stop indigo
```

```bash
# gorilla - "nuke" a whole region
docker-compose down
# bring it back up
docker-compose up
```

Note that your browser, not haproxy, provides reliability from multiple A records in DNS. As soon as your browser can't reach the address it was on, it switches, and will happily switch back if that one goes down.


TODO: Add stats here.
