# Get Secure

In this lab we will create a self-signed certificate and use it to SSL encrypt traffic between web browsers and HAProxy. Traffic between the proxy and servers remains unencrypted.

We will also put in a redirect so that any HTTP requests that come in are answered with a redirect to HTTPS.

Stop one of your clusters (either os or aws):

```bash
docker-compose down
```

Configure the other cluster

```bash
# Create the key, csr, and certificate in one shot
# credit: https://gist.github.com/PowerKiKi/02a90c765d7b79e7f64d
./generate-wildcard-certificate sofree.us
# combine them into a single-file for use with HAProxy
cat sofree.us.crt sofree.us.key > STAR.sofree.us.pem
# copy the pem file into the haproxy config folder
cp STAR.sofree.us.pem ~/sfs/aghi2haproxy/labs/rainbow/haproxy/
```

Add two lines to the haproxy.cfg. Don't add the plus signs, just the lines. When you're done, your haproxy.cfg should look like haproxy.cfg.secure in labs/.

```
dlwillson on BlackPearl.SoFree.Us in ~/sfs/aghi2haproxy/labs at 22:29:04
$ diff -C 3 haproxy.cfg.names haproxy.cfg.secure
*** haproxy.cfg.names	2020-05-15 22:17:21.376889797 -0600
--- haproxy.cfg.secure	2020-05-15 22:29:26.512057171 -0600
***************
*** 11,16 ****
--- 11,18 ----

  frontend main
      bind :80
+     bind :443 ssl crt /usr/local/etc/haproxy/STAR.sofree.us.pem
+     http-request redirect scheme https unless { ssl_fc }
      option httplog
      default_backend      rainbow
```

Restart haproxy

```bash
docker-compose restart haproxy
```

And browse and curl. Note that you are warned in the browser about the self-signed cert. To get cURL to accept the self-signed cert, provide the `-k` option.

```
$ curl https://alicorn.sofree.us
curl: (60) SSL certificate problem: self signed certificate
More details here: https://curl.haxx.se/docs/sslcerts.html

curl failed to verify the legitimacy of the server and therefore could not
establish a secure connection to it. To learn more about this situation and
how to fix it, please visit the web page mentioned above.
$ curl -k https://alicorn.sofree.us
<html>
...
```

Now configure your other cluster in the same way
