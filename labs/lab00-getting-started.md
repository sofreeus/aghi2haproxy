# Getting Started

Give your GitLab.com username to the teacher and wait for admins to be assigned to their machines. Then, start.

Everyone has two machines: os-$MACGUFFIN and aws-$MACGUFFIN

Secure shell to one of your machines and do the following:

```bash
/tmp/git-a-class aghi2haproxy
# if git-a-class isn't in /tmp/, you can download it from https://sofree.us/bits/git-a-class
cd ~/sfs/aghi2haproxy/labs/
./get-reqd
./get-reqd-user
sudo reboot
```

Wait 30 seconds and secure shell in again.

Take a moment to appreciate the practical beauty of Byobu, and then do these:

```bash
cd ~/sfs/aghi2haproxy/labs/rainbow/
vim create-index-files # set MACGUFFIN and REGION
./create-index-files
# Finally, bring up this region's servers and their proxy
docker-compose up -d
```

Perform both the above blocks on your other machine.

Now, browse and/or curl the following:
- $MACGUFFIN.sofree.us
- $REGION-$MACGUFFIN.sofree.us
- $COLOR-$REGION-$MACGUFFIN.sofree.us

In DNS, the regions are 'aws' and 'os', and the colors are ROYGBIV (red, orange, yellow, green, blue, indigo, violet).

Very good. You now have 2 machines, 2 regions, 2 proxies, and 14 web servers, or 1 clustered web-farm. Cool.

Let's look at the haproxy config together.

Browse or bash:

[haproxy.cfg](https://gitlab.com/sofreeus/aghi2haproxy/-/blob/master/labs/rainbow/haproxy/haproxy.cfg)

```bash
less ~/sfs/aghi2haproxy/labs/rainbow/haproxy/haproxy.cfg
```

https://www.haproxy.com/blog/introduction-to-haproxy-acls/
