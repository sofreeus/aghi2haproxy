# Maintenance Gate

You want to serve different content to QA engineers and regular clients.

For this example, the regular site will be taken down during the deployment and smoke testing. QA engineers will see the real, upgraded site, while regular browsers are sent to a maintenance page.

Consider shutting one region down while doing this lab. This is a hard lab and having two regions doubles the work, and adds little value.

Create a maintenance page.
TODO: pre-build this

Create a container to serve it.
TODO: pre-build this

In HAProxy:
- Add a maints-page backend
- Add a quality-source-ip ACL
- Add an rule to use the maints-page backend unless quality-source-ip

```
 frontend main
     bind :80
     bind :443 ssl crt /usr/local/etc/haproxy/STAR.sofree.us.pem
     http-request redirect scheme https unless { ssl_fc }
     option httplog
+    # this is wide open
+    acl network_allowed src 0.0.0.0/0
+    # this is filtered
+    # acl network_allowed src 67.42.246.126/32 72.46.60.58/32
+    use_backend maints unless network_allowed
     default_backend      rainbow

<snip>
     server indigo indigo:80  check
 backend violet
     server violet violet:80  check
+backend maints
+    server maints maints:80  check
```

See labs/haproxy.cfg.maints for reference

Bounce the whole stack and test. Start with no QA IPs, then add yours and your parters', and note the effect of each addition.

But, what's my IP?

Browse or cURL [ifconfig.me](https://ifconfig.me)

Add a /32 to make it CIDR.
