# Teacher Tools

This collection of files helps the teacher to configure the machines for class.

Each student gets two machines: One in Fuga Cloud and one in AWS.

To Do:
- Figure out how to set students up one at a time.
    * needs student-name and mcguffin
    * e.g. ./make-student-machines dlwillson alicorn
- Add City Cloud?
- Add Google Cloud?



1. Update macguffins
1. Create machines in Fuga Cloud and AWS
1. Update /etc/hosts with external IPs
1. Create DNS entry text from hosts
1. Update Gandi DNS with entries
1. Add students to their machines as admins
