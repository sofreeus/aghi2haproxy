# HAProxy, A Gentle Hands-on Introduction

What *is* HAProxy, even? It's software libre for load-balancing. So, it's a tool to help your web-farm be more performant and reliable, and an HTTP router, and a smart filter for your SMTP and SSH. It is the basis of many, if not most, of the load-balancer products in the world.

Like PostgreSQL, HAProxy is liberally licensed, so proprietary products can be made from it. SFS uses copyleft, but both copyleft and liberal are libre. In fact, as people that prefer liberal licenses will tell you, liberal is *more* free than copyleft. If BSD were not liberally licensed, there'd be no macOS! Anyway, enough about licenses.

HAProxy is fast, reliable, secure, and very, very free. It does HTTP and TCP proxy, stats, ACLs, all sorts of good things.


## Objectives

(what we will do and learn)

  * [ ] Stand up sixteen nodes: 1 cluster, 2 machines, 2 HAProxys, 14 HTTPd servers.
  * [ ] Configure SSL at HAProxy (i.e. SSL off-loading) and an HTTP to HTTPS redirect.
  * [ ] Stop some nodes.
  * [ ] Stop all the nodes in a region.
  * [ ] Put up a maintenance gate. (That's where we push a partially-tested change into production, and allow the QA engineers to complete testing, while everyone else gets a maintenance page.) This can be done TCP or HTTP. For this class, we'll probably use HTTP and wave at TCP.
  * [ ] Take down the maintenance gate.


## Requirements

(what you should bring)

- A workstation with ssh and a web-browser
- A GitLab.com account with an authorized ssh key
